## 2020-02-27

### Added (4h)
- Callback from keyboard to todo list
- Add missing actions (key combinations)
- Todo field validations
- Navigation by ArrowUp, ArrowDown, and actions Delete, Backspace, Enter are working
- Apply linter

## 2020-02-24

### Added (2.5h)
- Todo list
- Navigation by ArrowUp, ArrowDown, and actions Delete, Backspace, Enter are working


## 2020-02-22 (2.5h)

### Added
- Highlight pressed key on physical keyboard
- Switch language by Shift+Alt

### Changed
- Naming of control keys

## 2020-02-16 (1.5h)

### Added
- Caps is working
- Shift and Caps may work simultaneously

### Changed
- Work with key model, better control over primary and secondary chars, and actions for switch between them
- Way of keyboard rendering

## 2020-02-16 (2h)

### Added
- First keyboard layout with multilanguage support
- SHIFT is working


## 2020-02-15 (3h)
## Project init

### Added
- Keyboard module
- Install angular/material, angular/animations, angular/cdk
