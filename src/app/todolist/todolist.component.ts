import { Component, HostListener, ElementRef, ViewChild} from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Todo } from './models/Todo';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-todo-list',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodoListComponent {

  public todoFormControl = new FormControl('', [
    Validators.pattern(/^[a-zA-Zа-яА-Я0-9]{1,20}$/),
    Validators.maxLength(20)
  ]);

  todoList: Array<Todo> = [];

  activeTodo = -1;
  isEditMode = false;

  matcher = new MyErrorStateMatcher();

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    switch (event.key) {
      case 'Enter':
        this.enterAction();
        break;
      case 'ArrowDown':
        this.navigate(+1);
        break;
      case 'ArrowUp':
        this.navigate(-1);
        break;
      case 'Delete':
        if (this.todoFormControl.untouched || this.todoFormControl?.value?.length === 0) {
          this.deleteActive();
        }
        break;
      case 'Backspace':
        if (this.todoFormControl.untouched || this.todoFormControl?.value?.length === 0) {
          this.deleteActive();
        }
        break;
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
  }

  private enterAction() {
    if (this.activeTodo === -1 && !this.isEditMode && this.todoFormControl.status === 'VALID') {
      this.addToList();
    } else {
      if (this.isEditMode && this.todoFormControl.status === 'VALID') {
        this.updateOneFromList();
      } else if (this.activeTodo !== -1) {
        this.selectTodo();
      }
    }
  }

  private updateOneFromList() {
    this.todoList[this.activeTodo].text = this.todoFormControl.value;
    this.isEditMode = false;
    this.todoFormControl.reset();
    this.activeTodo = -1;
  }

  private addToList() {
    const value = this.todoFormControl.value?.trim();
    if (value) {
      this.todoList.push(new Todo(value));
      this.todoFormControl.reset();
    }
  }

  private deleteActive() {
    if (this.activeTodo !== -1 && this.todoList[this.activeTodo] && !this.isEditMode) {
      this.todoList.splice(this.activeTodo, 1);
    }

    if (this.activeTodo === this.todoList.length) {
      this.activeTodo--;
    }
  }

  private selectTodo() {
    this.isEditMode = true;
    this.todoFormControl.setValue(this.todoList[this.activeTodo].text);
  }

  private navigate(shift: number) {
    this.activeTodo += shift;
    this.isEditMode = false;
    if (this.activeTodo < 0 || this.todoList.length === 0) {
      this.activeTodo = -1;
    }
  }

  public isTodoActive(todo: Todo) {
    if (this.todoList[this.activeTodo]) {
      return todo.id === this.todoList[this.activeTodo].id;
    }

    return false;
  }

  public focusInput() {
    if (this.todoFormControl?.value?.length === 0) {
      this.activeTodo = -1;
    }
  }

  public focusTodo(index: number) {
    this.activeTodo = index;
    this.selectTodo();
  }

  public handleVirtKeyboardKey(keyVal: string) {
    switch (keyVal) {
      case 'Backspace':
        if (keyVal?.length === 1 && (this.todoFormControl.touched || this.todoFormControl?.value?.length > 0)) {
          this.todoFormControl.setValue(this.todoFormControl.value.slice(0, -1));
        } else {
          this.deleteActive();
        }
        break;
      case 'Enter':
        this.enterAction();
        break;
      default:
        if (keyVal?.length === 1 && (this.todoFormControl.touched || this.todoFormControl?.value?.length > 0)) {
          this.todoFormControl.setValue((this.todoFormControl?.value || '') + keyVal);
        }
        break;
    }
  }

}
