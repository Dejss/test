import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { TodoListComponent } from './todolist.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { KeyboardModule } from '../keyboard/keyboard.module';

@NgModule({
  declarations: [TodoListComponent],
  imports: [
    KeyboardModule,

    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [TodoListComponent],
  exports: [TodoListComponent]
})
export class TodoListModule { }
