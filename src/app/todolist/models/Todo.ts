import * as uuid from 'uuid';

export class Todo {
    private _id = '';
    private _text = '';

    constructor(text: string) {
        this._id = uuid.v4();
        this._text = text;
    }

    get id(): string  {
        return this._id;
    }

    set text(text: string) {
        this._text = text;
    }

    get text(): string {
        return this._text;
    }
}
