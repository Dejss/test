import Key from './Key';

class Keyboard {

    public static readonly availableLangauges: Array<string> = [
        'en',
        'ru'
    ];

    private static getStaticKeys(lang: string) {
        const staticKeyboard = {
            numbersPrimary: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'Backspace'],
            numbersSecondary: ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', 'Backspace'],
            controlsPrimary: ['Control', 'Alt', ' ', 'Alt', lang, 'Control', 'Delete'],
        };

        return {
            numbers: staticKeyboard.numbersPrimary.map((key: string, idx: number) => new Key(key, staticKeyboard.numbersSecondary[idx])),
            controls: staticKeyboard.controlsPrimary.map((key: string) => new Key(key, key))
        };
    }

    private static getMainKeys(lang: string) {
        const languages = {
            en: [
                ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'],
                ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'],
                ['z', 'x', 'c', 'v', 'b', 'n', 'm']
            ],
            ru: [
                ['й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', 'х', 'ъ'],
                ['ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э'],
                ['я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б ', 'ю']
            ]
        };

        const nonCharKeys = [
            {
                start: {
                    en: [new Key('Tab', 'Tab')],
                    ru: [new Key('Tab', 'Tab')]
                },
                end: {
                    en: [new Key('[', '{'), new Key('}', ']'), new Key('\\', '|')],
                    ru: [new Key('\\', '/')]
                }
            },
            {
                start: {
                    en: [new Key('CapsLock', 'CapsLock')],
                    ru: [new Key('CapsLock', 'CapsLock')]
                },
                end: {
                    en: [new Key(';', ':'), new Key('\'', ']'), new Key('Enter', 'Enter')],
                    ru: [new Key('Enter', 'Enter')],
                }
            },
            {
                start: {
                    en: [new Key('Shift', 'Shift')],
                    ru: [new Key('Shift', 'Shift')]
                },
                end: {
                    en: [new Key(',', '<'), new Key('.', '>'), new Key('/', '?'), new Key('Shift', 'Shift')],
                    ru: [new Key('.', ','), new Key('Shift', 'Shift')]
                }
            },
        ];

        return languages[lang].map((keyrow: Array<string>, idx: number) => {
            return nonCharKeys[idx].start[lang]
                .concat(keyrow.map(key => new Key(key, key.toLocaleUpperCase()))).concat(nonCharKeys[idx].end[lang]);
        });
    }

    public static getNewKeyboard(lang: string) {
        const staticKeyrows = this.getStaticKeys(lang);
        const languageKeyboard = this.getMainKeys(lang);
        const resultKeyboard = [];

        resultKeyboard.push(staticKeyrows.numbers);
        languageKeyboard.forEach((keyrow: Array<Key>) => resultKeyboard.push(keyrow));
        resultKeyboard.push(staticKeyrows.controls);

        return resultKeyboard;
    }

    public static checkLetters(key: Key) {
        return (key.primary >= 'a' && key.primary <= 'z') || (key.primary >= 'а' && key.primary <= 'я');
    }
}

export default Keyboard;
