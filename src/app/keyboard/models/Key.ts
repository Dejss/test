class Key {
    private _primary: string;
    private _secondary: string;

    constructor(primary: string, secondary: string) {
        this._primary = primary;
        this._secondary = secondary;
    }

    get primary() {
        return this._primary;
    }

    get secondary() {
        return this._secondary;
    }
}

export default Key;
