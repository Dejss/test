import { Component, HostListener, Output, EventEmitter } from '@angular/core';

import Keyboard from './models/Keyboard';
import Key from './models/Key';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.css']
})
export class KeyboardComponent {

  readonly wideKeys: Array<string> = ['CapsLock', 'Shift', 'Control', 'Delete', 'Backspace', 'Enter', 'Tab', ''];
  readonly extraWideKeys: Array<string> = [' '];

  readonly languages: Array<string> = Keyboard.availableLangauges;

  currLang = 0;

  currentAltState = false;
  currentShiftState = false;
  currentCapsState = false;
  currentControlState = false;

  currentActiveKeys: Array<string> = [];

  currKeyboard: Array<Array<Key>> = Keyboard.getNewKeyboard(this.languages[this.currLang]);

  @Output() keyClickedCallback: EventEmitter<any> = new EventEmitter();

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    switch (event.key) {
      case 'Shift':
        this.currentShiftState = true;
        this.currentActiveKeys = [];
        if (this.currentAltState || this.currentControlState) {
          this.changeLanguage();
        }
        break;
      case 'CapsLock':
        this.currentCapsState = !this.currentCapsState;
        this.currentActiveKeys = [];
        break;
      case 'Alt':
        this.currentAltState = !this.currentAltState;
        if (this.currentShiftState) {
          this.changeLanguage();
        }
        break;
      case 'Control':
        this.currentControlState = !this.currentControlState;
        if (this.currentShiftState) {
          this.changeLanguage();
        }
        break;
      default:
        if (this.currentActiveKeys.indexOf(event.key) === -1) {
          this.currentActiveKeys.push(event.key);
        }
    }
  }

  @HostListener('window:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
    switch (event.key) {
      case 'Shift':
        this.currentShiftState = false;
        this.currentActiveKeys = [];
        break;
      case 'Control':
        this.currentControlState = !this.currentControlState;
        break;
      default:
        const index = this.currentActiveKeys.indexOf(event.key);
        if (index !== -1) {
          this.currentActiveKeys.splice(index, 1);
        }
    }
  }

  private _handleCapsMode(key: Key) {
    if (Keyboard.checkLetters(key)) {
      return this.currentShiftState ? key.primary : key.secondary;
    }

    return this.currentShiftState ? key.secondary : key.primary;
  }

  public keyClicked(key: Key) {
    if (this.languages.indexOf(key.primary) !== -1) {
      this.changeLanguage();
      return;
    }
    if (key.primary === 'Shift') {
      this.currentShiftState = !this.currentShiftState;
    }
    if (key.primary === 'Alt') {
      this.currentAltState = !this.currentAltState;
    }
    if (key.primary === 'CapsLock') {
      this.currentCapsState = !this.currentCapsState;
    }
    if (key.primary === 'Control') {
      this.currentControlState = !this.currentControlState;
    }

    if ((key.primary === 'a' || key.primary === 'ф') && this.currentControlState) {
      document.activeElement.ownerDocument.execCommand('selectAll');
      return;
    }

    if ((key.primary === 'c' || key.primary === 'с') && this.currentControlState) {
      document.activeElement.ownerDocument.execCommand('copy');
      return;
    }

    if ((key.primary === 'v' || key.primary === 'м') && this.currentControlState) {
      document.activeElement.ownerDocument.execCommand('paste');
      return;
    }

    if ((key.primary === 'x' || key.primary === 'ч') && this.currentControlState) {
      document.activeElement.ownerDocument.execCommand('cut');
      return;
    }

    if (this.currentControlState && this.currentShiftState) {
      this.changeLanguage();
      this.currentControlState = this.currentShiftState = false;
    }

    if (this.currentAltState && this.currentShiftState) {
      this.changeLanguage();
      this.currentAltState = this.currentShiftState = false;
    }

    this.keyClickedCallback.emit(this.currentCapsState && Keyboard.checkLetters(key)
      || this.currentShiftState ? key.secondary : key.primary);
  }

  private changeLanguage() {
    this.currLang = ++this.currLang > this.languages.length - 1 ? 0 : this.currLang;
    this.currKeyboard = Keyboard.getNewKeyboard(this.languages[this.currLang]);
  }

  public renderKey(key: Key) {
    if (this.currentCapsState) {
      return this._handleCapsMode(key);
    }
    if (this.currentShiftState) {
      return key.secondary;
    }
    return key.primary;
  }

  public renderActiveKey(key: Key) {
    if (key.primary === 'Shift' && this.currentShiftState) {
      return true;
    } else if (key.primary === 'CapsLock' && this.currentCapsState) {
      return true;
    } else if (key.primary === 'Control' && this.currentControlState) {
      return true;
    } else if (key.primary === 'Alt' && this.currentAltState) {
      return true;
    }

    if (this.currentCapsState && Keyboard.checkLetters(key) || this.currentShiftState) {
      return this.currentActiveKeys.indexOf(key.secondary) === -1 ? false : true;
    } else {
      return this.currentActiveKeys.indexOf(key.primary) === -1 ? false : true;
    }
  }
}
