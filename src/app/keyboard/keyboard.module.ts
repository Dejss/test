import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { NgModule } from '@angular/core';
import { KeyboardComponent } from './keyboard.component';

@NgModule({
  declarations: [KeyboardComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [KeyboardComponent],
  exports: [KeyboardComponent]
})
export class KeyboardModule { }
